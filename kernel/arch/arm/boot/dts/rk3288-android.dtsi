/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */
#include <dt-bindings/pwm/pwm.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/soc/rockchip-system-status.h>
#include "rk3288-dram-default-timing.dtsi"
#include <dt-bindings/display/media-bus-format.h>

/ {
	firmware {
		android {
			compatible = "android,firmware";
			fstab {
				compatible = "android,fstab";
				system {
					compatible = "android,system";
					dev = "/dev/block/by-name/system";
					type = "ext4";
					mnt_flags = "rw,barrier=1,inode_readahead_blks=8";
					fsmgr_flags = "wait";
				};
				vendor {
					compatible = "android,vendor";
					dev = "/dev/block/by-name/vendor";
					type = "ext4";
					mnt_flags = "rw,barrier=1,inode_readahead_blks=8";
					fsmgr_flags = "wait";
				};
			};
		};
	};
	chosen {
		bootargs = "earlycon=uart8250,mmio32,0xff690000 vmalloc=496M";
	};

	cpuinfo {
		compatible = "rockchip,cpuinfo";
		nvmem-cells = <&efuse_id>;
		nvmem-cell-names = "id";
	};

	/delete-node/ dmc@ff610000;

	dfi: dfi {
		compatible = "rockchip,rk3288-dfi";
		rockchip,pmu = <&pmu>;
		rockchip,grf = <&grf>;
		status = "disabled";
	};

	dmc: dmc {
		compatible = "rockchip,rk3288-dmc";
		devfreq-events = <&dfi>;
		clocks = <&cru SCLK_DDRCLK>, <&cru PCLK_PUBL0>,
			 <&cru PCLK_DDRUPCTL0>, <&cru PCLK_PUBL1>,
			 <&cru PCLK_DDRUPCTL1>;
		clock-names = "dmc_clk", "pclk_phy0", "pclk_upctl0",
			      "pclk_phy1", "pclk_upctl1";
		upthreshold = <55>;
		downdifferential = <10>;
		operating-points-v2 = <&dmc_opp_table>;
		vop-dclk-mode = <0>;
		min-cpu-freq = <600000>;
		rockchip,ddr_timing = <&ddr_timing>;
		system-status-freq = <
			/*system status		freq(KHz)*/
			SYS_STATUS_NORMAL	396000
			SYS_STATUS_REBOOT	396000
			SYS_STATUS_SUSPEND	192000
			SYS_STATUS_VIDEO_1080P	300000
			SYS_STATUS_VIDEO_4K	396000
			SYS_STATUS_VIDEO_4K_10B	528000
			SYS_STATUS_PERFORMANCE	528000
			SYS_STATUS_BOOST	396000
			SYS_STATUS_DUALVIEW	396000
			SYS_STATUS_ISP		396000
		>;
		auto-min-freq = <396000>;
		auto-freq-en = <0>;
		status = "diasbled";
	};

	dmc_opp_table: opp_table2 {
		compatible = "operating-points-v2";

		opp-192000000 {
			opp-hz = /bits/ 64 <192000000>;
			opp-microvolt = <1100000>;
		};
		opp-300000000 {
			opp-hz = /bits/ 64 <300000000>;
			opp-microvolt = <1100000>;
		};
		opp-396000000 {
			opp-hz = /bits/ 64 <396000000>;
			opp-microvolt = <1100000>;
		};
		opp-528000000 {
			opp-hz = /bits/ 64 <528000000>;
			opp-microvolt = <1150000>;
		};
	};

	reserved-memory {
		ramoops_mem: ramoops@00000000 {
			reg = <0x0 0x8000000 0x0 0xF0000>;
		};

		drm_logo: drm-logo@00000000 {
		compatible = "rockchip,drm-logo";
			reg = <0x0 0x0 0x0 0x0>;
		};
	};

	ramoops {
		compatible = "ramoops";
		record-size = <0x0 0x20000>;
		console-size = <0x0 0x80000>;
		ftrace-size = <0x0 0x00000>;
		pmsg-size = <0x0 0x50000>;
		memory-region = <&ramoops_mem>;
	};


	psci {
		compatible = "arm,psci-1.0";
		method = "smc";
	};

	/delete-node/ timer@ff810000;

	dwc_control_usb: dwc-control-usb@ff770284 {
		compatible = "rockchip,rk3288-dwc-control-usb";
		status = "okay";
		reg = <0x0 0xff770284 0x0 0x04>, <0x0 0xff770288 0x0 0x04>,
		      <0x0 0xff7702cc 0x0 0x04>, <0x0 0xff7702d4 0x0 0x04>,
		      <0x0 0xff770320 0x0 0x14>, <0x0 0xff770334 0x0 0x14>,
		      <0x0 0xff770348 0x0 0x10>, <0x0 0xff770358 0x0 0x08>,
		      <0x0 0xff770360 0x0 0x08>;
		reg-names = "GRF_SOC_STATUS1" ,"GRF_SOC_STATUS2",
			    "GRF_SOC_STATUS19", "GRF_SOC_STATUS21",
			    "GRF_UOC0_BASE", "GRF_UOC1_BASE",
			    "GRF_UOC2_BASE", "GRF_UOC3_BASE",
			    "GRF_UOC4_BASE";
		interrupts = <GIC_SPI 93 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 94 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 95 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 96 IRQ_TYPE_LEVEL_HIGH>,
			     <GIC_SPI 97 IRQ_TYPE_LEVEL_HIGH>;
		interrupt-names = "otg_id", "otg_bvalid",
				  "otg_linestate", "host0_linestate",
				  "host1_linestate";
		clocks = <&cru HCLK_USB_PERI>;
		clock-names = "hclk_usb_peri";

		otg_drv_gpio = <&gpio0 12 GPIO_ACTIVE_HIGH>;
		rockchip,remote_wakeup;
		rockchip,usb_irq_wakeup;

		usb_bc {
			compatible = "synopsys,phy";
			rk_usb,bvalid     = <0x288 14 1>;
			rk_usb,iddig      = <0x288 17 1>;
			rk_usb,dcdenb     = <0x328 14 1>;
			rk_usb,vdatsrcenb = <0x328  7 1>;
			rk_usb,vdatdetenb = <0x328  6 1>;
			rk_usb,chrgsel    = <0x328  5 1>;
			rk_usb,chgdet     = <0x2cc 23 1>;
			rk_usb,fsvminus   = <0x2cc 25 1>;
			rk_usb,fsvplus    = <0x2cc 24 1>;
		};
	};

	nandc0: nandc@ff400000 {
		compatible = "rockchip,rk-nandc";
		reg = <0x0 0xff400000 0x0 0x4000>;
		interrupts = <GIC_SPI 38 IRQ_TYPE_LEVEL_HIGH>;
		nandc_id = <0>;
		clocks = <&cru SCLK_NANDC0>, <&cru HCLK_NANDC0>;
		clock-names = "clk_nandc", "hclk_nandc";
		status = "okay";
	};
};

&dfi {
	status = "okay";
};

&dmc {
	center-supply = <&vdd_log>;
	status = "okay";
};
&cpu0 {
	enable-method = "psci";
	cpu0-supply = <&vdd_cpu>;
};

&cpu0_opp_table {
	rockchip,avs-enable = <1>;
	opp-1800000000 {
		opp-hz = /bits/ 64 <1800000000>;
		opp-microvolt = <1350000 1350000 1350000>;
		opp-microvolt-L0 = <1350000 1350000 1350000>;
		opp-microvolt-L1 = <1350000 1350000 1350000>;
		opp-microvolt-L2 = <1350000 1350000 1350000>;
		opp-microvolt-L3 = <1350000 1350000 1350000>;
		clock-latency-ns = <40000>;
		status = "disabled";
	};
};

&cpu1 {
	enable-method = "psci";
};

&cpu2 {
	enable-method = "psci";
};

&cpu3 {
	enable-method = "psci";
};

&dmac_bus_s {
	// change to non-secure dmac
	reg = <0x0 0xff600000 0x0 0x4000>;
};

&efuse {
	compatible = "rockchip,rk3288-secure-efuse";
};

&iep {
	status = "okay";
};

&iep_mmu {
	status = "okay";
};

&rga {
	compatible = "rockchip,rga2";
	clocks = <&cru ACLK_RGA>, <&cru HCLK_RGA>, <&cru SCLK_RGA>;
	clock-names = "aclk_rga", "hclk_rga", "clk_rga";
	dma-coherent;
};

&usb_otg {
	compatible = "rockchip,rk3288_usb20_otg";
	clocks = <&usbphy0>, <&cru HCLK_OTG0>;
	clock-names = "clk_usbphy0", "hclk_usb0";
	resets = <&cru SRST_USBOTG_AHB>,
		 <&cru SRST_USBOTG_PHY>,
		 <&cru SRST_USBOTG_CON>;
	reset-names = "otg_ahb", "otg_phy", "otg_controller";
	/*0 - Normal, 1 - Force Host, 2 - Force Device*/
	rockchip,usb-mode = <0>;
};

&usb_otg {
	status = "okay";
};
